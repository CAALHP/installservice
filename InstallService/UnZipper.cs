using System;
using System.IO;
using Ionic.Zip;

namespace InstallService
{
    public static class UnZipper
    {
        public static void UnZip(string pathtofile, string targetdir)
        {
            using (var zip = ZipFile.Read(pathtofile))
            {
                // This call to ExtractAll() assumes:
                //   - none of the entries are password-protected.
                //   - want to extract all entries to current working directory
                //   - none of the files in the zip already exist in the directory;
                //     if they do, the method will throw.
                if (zip != null)
                {

                    try
                    {
                        var dir = new DirectoryInfo(targetdir + Path.GetFileNameWithoutExtension(pathtofile));
                        if (dir.Exists)
                            CleanDirectory(new DirectoryInfo(targetdir + Path.GetFileNameWithoutExtension(pathtofile)));
                        zip.ExtractExistingFile = ExtractExistingFileAction.OverwriteSilently;
                        zip.ExtractAll(targetdir);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
                else
                {
                    throw new NullReferenceException("Reading zip file failed!");
                }
            }
        }

        public static void CleanDirectory(DirectoryInfo targetdir)
        {
            //recursively clean subdirectories
            foreach (var subdir in targetdir.EnumerateDirectories())
            {
                CleanDirectory(subdir);
            }
            //clean files from this directory
            foreach (var file in targetdir.EnumerateFiles()) //.Where(file => file.Extension.ToLower().Equals(".tmp")))
            {
                file.Delete();
            }
        }
    }
}