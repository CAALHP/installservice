using System.IO;
using System.Media;
using System.Reflection;

namespace InstallService
{
    public static class InstallSoundPlayer
    {
        /// <summary>
        /// Remember to document everything
        /// Play a sound when install completes
        /// </summary>
        public static void PlayInstallCompleteSound()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var dir = Path.GetDirectoryName(assembly.Location);
            if (dir == null) return;
            var soundfile = Path.Combine(dir, "complete.wav");
            var player = new SoundPlayer(soundfile);
            player.Play();
        }
    }
}