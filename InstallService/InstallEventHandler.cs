﻿using caalhp.Core.Events;
using System.IO;

namespace InstallService
{
    public class InstallEventHandler : IHandleEvent
    {
        private readonly IInstallStrategy _installStrategy;

        public InstallEventHandler(IInstallStrategy installStrategy)
        {
            _installStrategy = installStrategy;
        }

        public void HandleEvent(dynamic obj)
        {
            Handle(obj);
        }

        private async void Handle(DownloadAppCompletedEvent e)
        {
            await _installStrategy.InstallApp(e);
         
        }

        private async void Handle(DownloadDeviceDriverCompletedEvent e)
        {
            var lastIndex = e.DeviceDriverFileName.LastIndexOf(@"driver");
            var extensionIndex = e.DeviceDriverFileName.LastIndexOf(@".zip");
            var findLastName = e.DeviceDriverFileName.Substring(lastIndex, extensionIndex-lastIndex);
            await _installStrategy.InstallDeviceDriver(e.DeviceDriverFileName, findLastName);
        }
    }
}