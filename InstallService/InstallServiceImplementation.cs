﻿using System;
using System.Collections.Generic;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;

namespace InstallService
{
    public class InstallServiceImplementation : IServiceCAALHPContract
    {
        private const string Name = "InstallService";
        private IServiceHostCAALHPContract _serviceHost;
        private int _processId;
        private readonly IHandleEvent _installEventHandler;

        public IServiceHostCAALHPContract ServiceHost { get { return _serviceHost; } }
        public int ProcessId { get { return _processId; } }

        public InstallServiceImplementation()
        {
            IInstallStrategy installer = new InstallStrategy(this);
            _installEventHandler = new InstallEventHandler(installer);
        }

        #region IServiceCAALHPContract functions

        /// <summary>
        /// Handle event notifications 
        /// </summary>
        /// <param name="notification">The notification object that the host sends to the subscribing plugin</param>
        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            _installEventHandler.HandleEvent(obj);
        }

        /// <summary>
        /// Default method for getting the name of the plugin
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            return Name;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        /// <summary>
        /// Initialize the hostobject
        /// </summary>
        /// <param name="hostObj"></param>
        /// <param name="processId"></param>
        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _serviceHost = hostObj;
            _processId = processId;
            //This plugin needs to subscribe to events from the host
            //_host.Host.SubscribeToEvents(_processId);
            _serviceHost.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadAppCompletedEvent)), _processId);
            _serviceHost.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadDeviceDriverCompletedEvent)), _processId);
        }

        public void Start()
        {
            //throw new System.NotImplementedException();
        }

        public void Stop()
        {
            //throw new System.NotImplementedException();
        }

        #endregion





    }
}