namespace InstallService
{
    public interface IHandleEvent
    {
        void HandleEvent(dynamic obj);
    }
}