﻿using System;
using System.Windows.Forms;
using caalhp.IcePluginAdapters;
using Exception = Ice.Exception;

namespace InstallService
{
    public class Program
    {
        private static ServiceAdapter _adapter;
        private static InstallServiceImplementation _implementation;

        static void Main(string[] args)
        {
            const string endpoint = "localhost";
            try
            {
                _implementation = new InstallServiceImplementation();
                _adapter = new ServiceAdapter(endpoint, _implementation);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            Console.WriteLine("Press <ENTER> to exit program.");
            Console.ReadLine();
        }
    }
}
