﻿using System.Threading;
using System.Threading.Tasks;
using caalhp.Core.Events;

namespace InstallService
{
    public interface IInstallStrategy
    {
        Task InstallAppAsync(string filename, string appname, CancellationToken cancellationToken);
        Task InstallDeviceDriverAsync(string filename, string drivername, CancellationToken cancellationToken);
        Task InstallServiceAsync(string filename, string servicename, CancellationToken cancellationToken);

        /// <summary>
        /// Install a device driver async
        /// </summary>
        /// <param name="filename"></param>
        Task InstallDeviceDriver(string filename, string drivername);

        /// <summary>
        /// Install an app async
        /// </summary>
        /// <param name="e">A DownloadAppCompletedEvent</param>
        Task InstallApp(DownloadAppCompletedEvent e);
    }
}
