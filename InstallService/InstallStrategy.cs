﻿using System;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;

namespace InstallService
{
    public class InstallStrategy : IInstallStrategy
    {
        private readonly InstallServiceImplementation _installServiceImplementation;
        private readonly string _downloadDir;
        private readonly string _targetDir;
        //private readonly IDeviceDriverHost _host;
        private const string AppDir = "Apps/";
        private const string DeviceDriverDir = "DeviceDrivers/";
        private const string ServicesDir = "Services/";

        public InstallStrategy(InstallServiceImplementation installServiceImplementation)
        {
            _installServiceImplementation = installServiceImplementation;
            _downloadDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings.Get("downloadDirectory"));
            _targetDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings.Get("pluginDirectory"));
        }

        public async Task InstallAppAsync(string filename, string appname, CancellationToken cancellationToken)
        {
            var task = Task.Run(() => UnZipper.UnZip(_downloadDir + filename, Path.Combine(_targetDir, AppDir, appname)), cancellationToken);
            try
            {
                await task;
            }
            catch (AggregateException ae)
            {
                Console.WriteLine(ae);
                throw;
            }
        }

        public async Task InstallDeviceDriverAsync(string filename, string drivername, CancellationToken cancellationToken)
        {
            filename = Path.GetFileName(filename);
            var task = Task.Run(() => UnZipper.UnZip(_downloadDir + filename, Path.Combine(_targetDir, DeviceDriverDir, drivername)), cancellationToken);
            try
            {
                await task;
            }
            catch (AggregateException ae)
            {
                Console.WriteLine(ae);
                throw;
            }
        }

        public async Task InstallServiceAsync(string filename, string servicename, CancellationToken cancellationToken)
        {
            var task = Task.Run(() => UnZipper.UnZip(_downloadDir + filename, Path.Combine(_targetDir, ServicesDir, servicename)), cancellationToken);
            try
            {
                await task;
            }
            catch (AggregateException ae)
            {
                Console.WriteLine(ae);
                throw;
            }
        }

        /// <summary>
        /// Install a device driver async
        /// </summary>
        /// <param name="filename"></param>
        public async Task InstallDeviceDriver(string filename, string drivername)
        {
            try
            {
                await InstallDeviceDriverAsync(filename, drivername, CancellationToken.None);
                //Report success
                //var command = EventHelper.CreateEvent(_processId, GetName(), "InstallDeviceDriverCompleted", filename);
                var command = new InstallDeviceDriverCompletedEvent
                {
                    CallerName = _installServiceImplementation.GetName(),
                    CallerProcessId = _installServiceImplementation.ProcessId,
                    DeviceDriverFileName = filename
                };
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
                _installServiceImplementation.ServiceHost.Host.ReportEvent(serializedCommand);
                InstallSoundPlayer.PlayInstallCompleteSound();
                _installServiceImplementation.ServiceHost.ActivateDeviceDrivers();
            }
            catch (Exception ex)
            {
                //Report error to the host
                //var command = EventHelper.CreateEvent(_processId, GetName(), "InstallDeviceDriverFailed", filename);
                var command = new InstallDeviceDriverFailedEvent
                {
                    CallerName = _installServiceImplementation.GetName(),
                    CallerProcessId = _installServiceImplementation.ProcessId,
                    DeviceDriverFileName = filename,
                    Reason = ex.Message
                };
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
                _installServiceImplementation.ServiceHost.Host.ReportEvent(serializedCommand);
            }
        }

        /// <summary>
        /// Install an app async
        /// </summary>
        /// <param name="e">A DownloadAppCompletedEvent</param>
        public async Task InstallApp(DownloadAppCompletedEvent e)
        {
            var filename = e.FileName;
            try
            {

                _installServiceImplementation.ServiceHost.CloseApp(Path.GetFileNameWithoutExtension(filename));
                await InstallAppAsync(filename, e.AppName, CancellationToken.None);

                var command = new InstallAppCompletedEvent
                {
                    CallerName = _installServiceImplementation.GetName(),
                    CallerProcessId = _installServiceImplementation.ProcessId,
                    FileName = filename,
                    AppName = e.AppName
                };
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);

                _installServiceImplementation.ServiceHost.Host.ReportEvent(serializedCommand);
                InstallSoundPlayer.PlayInstallCompleteSound();
            }
            catch (Exception ex)
            {
                var command = new InstallAppFailedEvent
                {
                    CallerName = _installServiceImplementation.GetName(),
                    CallerProcessId = _installServiceImplementation.ProcessId,
                    AppFileName = filename,
                    Reason = ex.Message
                };
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
                _installServiceImplementation .ServiceHost.Host.ReportEvent(serializedCommand);
            }
        }
    }
}
